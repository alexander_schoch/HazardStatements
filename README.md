# Hazard Statements
A software to automatically get all H and P statements at full length with just the unsorted and redundant acronyms as input

## How to use

- Copy/Paste all H and P statements into the `input.txt` file (get the acronyms from https://pubchem.ncbi.nlm.nih.gov/).
- Run the program. Note that the input file has to be named `input.txt`. Otherwise, there will be an error.</li>
- The statements are ready for copy-paste in the file `output.txt`.

### Input Format
- All statements have to be on separate lines without any commas or similar. `inputExample.txt` is an example file (shocker!). Hint:
  - Copy all statements into the file
  - find and replace: replace "and" with ","
  - find and replace: replace "+" with ","
  - find and replace: replace "," with "\n" (DuckDuckGo says that `\r\n` does the trick on windows)
  - find and replace: replace " " with ""
- The statements do not have to be sorted
- There can be duplicate lines


## Installing/Compiling

### Linux
- NOTE: the `git` package has to be installed.
- Open Terminal - `cd` into your desired folder
- type `git clone https://gitlab.com/alexander_schoch/hazardstatements.git`
- type `cd hazardstatements`
- type `g++ hazardStatements.cpp`
- edit `input.txt` with `gedit input.txt`
- run the program with `./a.out`

### Mac
- Click on "Clone or download" - "Download zip"
- Unzip it
- Open Terminal - `cd` into the extracted folder
- edit `input.txt` (with newline at the end)
- type `g++ hazardStatements.cpp`
- run the program with `./a.out`
- NOTE: Make sure to add a newline after the last acronym. Otherwise, the last statement will be ignored. (TextEdit does not automatically make one like most other editors)

### Windows
NOTE: @Nereo, @Dana: Thank you so much for compiling the software for windows. Ech zahl üch de mol eis.
Two versions = double chance to work!
- Click on "Clone or download" - "Download zip"
- Unzip it
- edit `input.txt`
- run `hazardStatements_Dana.exe` or `hazardStatements_Nereo.exe` (maybe as an administrator)
