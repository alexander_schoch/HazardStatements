H225 – Highly flammable liquid and vapour.
H319 – Causes serious eye irritation.
H336 – May cause drowsiness or dizziness.
P210 – Keep away from heat/sparks/open flames/hot surfaces. — No smoking.
P222 – Do not allow contact with air.
P223 – Keep away from any possible contact with water, because of violent reaction and possible flash fire.
P231 – Handle under inert gas.
P232 – Protect from moisture.
P233 – Keep container tightly closed.
P235 – Keep cool.
P240 – Ground/bond container and receiving equipment.
P241 – Use explosion-proof electrical/ventilating/lighting/…/equipment.
P242 – Use only non-sparking tools.
P243 – Take precautionary measures against static discharge.
P260 – Do not breathe dust/fume/gas/mist/vapours/spray.
P261 – Avoid breathing dust/fume/gas/mist/vapours/spray.
P264 – Wash … thoroughly after handling.
P271 – Use only outdoors or in a well-ventilated area.
P273 – Avoid release to the environment.
P280 – Wear protective gloves/protective clothing/eye protection/face protection.
P302 – IF ON SKIN:
P303 – IF ON SKIN (or hair):
P304 – IF INHALED:
P311 – Call a POISON CENTER or doctor/physician.
P313 – Get medical advice/attention.
P314 – Get medical advice/attention if you feel unwell.
P321 – Specific treatment (see … on this label).
P332 – If skin irritation occurs:
P340 – Remove victim to fresh air and keep at rest in a position comfortable for breathing.
P352 – Wash with plenty of soap and water.
P353 – Rinse skin with water/shower.
P361 – Remove/Take off immediately all contaminated clothing.
P362 – Take off contaminated clothing and wash before reuse.
P370 – In case of fire:
P378 – Use … for extinction.
P391 – Collect spillage.
P403 – Store in a well-ventilated place.
P405 – Store locked up.
P501 – Dispose of contents/container to …
