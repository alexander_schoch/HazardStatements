#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using std::string;
using namespace std;

/*
  NOTE: there are 62 H statements and 104 P statements.
 */

#define num_H 62
#define num_P 104
#define length_st 4
#define MAX_LIN 128

string H[num_H];
string P[num_P];
vector<string> used;
vector<string> usedFull;

void fillDB ();
void fillUsed ();
void fillUsedFull(string strUsed);
bool isMatching (const char *charUsed, const char *charH);
void printOutput();

int main()
{
  fillDB();
  fillUsed();

  for(unsigned int i = 0; i < used.size(); i++)
  {
    fillUsedFull(used.at(i));
  }

  std::sort (usedFull.begin(), usedFull.end());

  /*
  auto last = std::unique(usedFull.begin(), usedFull.end());
  usedFull.erase(last, usedFull.end());
  */

  usedFull.erase( unique( usedFull.begin(), usedFull.end() ), usedFull.end() );

  printOutput();
  return 0;
}

void printOutput()
{
  ofstream out ("output.txt");
  for(unsigned int i = 0; i < usedFull.size(); i++)
  {
    string line = usedFull.at(i);
    cout << line << endl;
    out << line << endl;
  }
  out.close();
  return;
}

void fillUsedFull(string strUsed)
{
  const char *charUsed = strUsed.c_str();
  for(int j = 0; j < num_H; j++)
  {
    string strH = H[j];
    const char *charH = strH.c_str();
    if(isMatching(charUsed, charH))
    {
      usedFull.push_back(strH);
    }
  }

  for(int l = 0; l < num_P; l++)
  {
    string strP = P[l];
    const char *charP = strP.c_str();
    if(isMatching(charUsed, charP))
    {
      usedFull.push_back(strP);
    }
  }

  return;
}

bool isMatching (const char *charUsed, const char *charH)
{
  for(int k = 0; k < length_st; k++)
  {
    if(charUsed[k] != charH[k]) return false;
  }
  return true;
}

void fillDB ()
{
  ifstream db_H ("H.txt");
  ifstream db_P ("P.txt");
  char line[MAX_LIN];
  char line2[MAX_LIN];
  int index = 0, index2 = 0;

  db_H.getline (line, MAX_LIN);
  db_P.getline (line2, MAX_LIN);
  while(!db_H.eof ())
  {
    H[index] = line;
    index++;
    db_H.getline (line, MAX_LIN);
  }

  while(!db_P.eof ())
  {
    P[index2] = line2;
    index2++;
    db_P.getline (line2, MAX_LIN);
  }

  db_H.close();
  db_P.close();
  return;
}

void fillUsed()
{

    ifstream db_used ("input.txt");
    char line[MAX_LIN];
    db_used.getline(line, MAX_LIN);
    while(!db_used.eof ())
    {
      used.push_back(line);
      db_used.getline(line, MAX_LIN);
    }
    db_used.close();
    return;
}
