#!/bin/bash

H=H.txt
P=P.txt

INPUT=$1
INPUT_TMP=input_tmp.txt

OUTPUT="hazards.tex"

sort "$INPUT" | uniq > $INPUT_TMP

echo "\begin{itemize}" > "$OUTPUT"

while read ln 
do
  t=$( echo "$ln" | head -c 1 )
  statement=$( grep "$ln" "$t.txt" | tail -c +8 )
  echo "  \item $ln -- $statement" >> "$OUTPUT"
done < "$INPUT_TMP"

echo "\end{itemize}" >> "$OUTPUT"

rm "$INPUT_TMP"
